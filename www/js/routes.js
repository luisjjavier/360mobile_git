angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
      $stateProvider
        .state('login', {
          url: "/login",
          templateUrl: "templates/login.html",
          controller: 'loginCtrl'

        })
        .state('actasAlternativas', {
          url: "/actasAlternativas",
          cache : false,
          templateUrl: "templates/actasAlternativas.html",
          controller :'actasAlternativasCtrl'
        })
        .state('actaOficial', {
    url: "/actasOficiales",
    cache : false,
    templateUrl: "templates/actasOficiales.html",
    controller :'actasOficialesCtrl'
  })
        .state('menu', {
    url: "/menu",
    cache : false,
    templateUrl: "templates/menu.html",
    controller :'menuCtrl'
  }).state('concurrentes', {
    url: "/concurrentes",
    cache : false,
    templateUrl: "templates/concurrentes.html",
    controller :'concurrentesCtrl'
  });;

  $urlRouterProvider.otherwise('/menu');


});
