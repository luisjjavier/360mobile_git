angular.module('app.controllers', [])

.controller('loginCtrl', function($scope) {

})

.controller('registroDeElectoresCtrl', function($scope) {

}).controller('actasAlternativasCtrl',function ($scope,$ionicPopup){
   $scope.parties = {

   };
  $scope.enviar = function () {
      console.log($scope.parties.Municipio)
      var message = "Colegio enviado exitosamente...!!";
      var title = 'Enviado <i class="icon ion-android-checkbox-outline"></i>'
      var valid = true;
      if($scope.parties.Municipio <=0) {
         message = "Codigo de municipio incorrecto";
         title = 'Error <i style="color:#ff3b30" class="icon ion-close-circled"></i>';
        valid = false;
      }else if ($scope.parties.Municipio <=0){
        message = "Colegio incorrecto";
        title = 'Error <i style="color:#ff3b30" class="icon ion-close-circled"></i>'
      }else if ($scope.parties.Municipio <=0){
        message = "Codigo Verificador incorrecto.";
        title = 'Error <i style="color:#ff3b30" class="icon ion-close-circled"></i>';
        valid = false;
      } else if  (!validVotes()) {
        message = "Total de votos incorrecto.";
        title = 'Error <i style="color:#ff3b30" class="icon ion-close-circled"></i>';
        valid = false;
      }
      var alertPopup = $ionicPopup.alert({
        title: title,
        template: message
      });

      alertPopup.then(function(res) {
        if ( valid ) {
          console.log('Se ha enviado el acta alternativa.');
          $scope.parties.PLD="";
          $scope.parties.PRD="";
          $scope.parties.PRSC="";
          $scope.parties.Valid ="";
        }
      });

  }
  function validVotes() {
    return $scope.parties.PLD + $scope.parties.PRD + $scope.parties.PRSC == $scope.parties.Valid
  }
  $scope.cleanForm = function () {
    $scope.parties = {}
  }

}).controller('actasOficialesCtrl',function ($scope,$ionicPopup){
  $scope.parties = {

  };
  $scope.enviar = function () {
    console.log($scope.parties.Municipio)
    var message = "Colegio enviado exitosamente...!!";
    var title = 'Enviado <i class="icon ion-android-checkbox-outline"></i>'
    var valid = true;
    if($scope.parties.Municipio <=0) {
      message = "Codigo de municipio incorrecto";
      title = 'Error <i style="color:#ff3b30" class="icon ion-close-circled"></i>';
      valid = false;
    }else if ($scope.parties.Municipio <=0){
      message = "Colegio incorrecto";
      title = 'Error <i style="color:#ff3b30" class="icon ion-close-circled"></i>'
    }else if ($scope.parties.Municipio <=0){
      message = "Codigo Verificador incorrecto.";
      title = 'Error <i style="color:#ff3b30" class="icon ion-close-circled"></i>';
      valid = false;
    } else if  (!validVotes()) {
      message = "Total de votos incorrecto.";
      title = 'Error <i style="color:#ff3b30" class="icon ion-close-circled"></i>';
      valid = false;
    }
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message
    });

    alertPopup.then(function(res) {
      if ( valid ) {
        console.log('Se ha enviado el acta alternativa.');
        $scope.parties.PLD="";
        $scope.parties.PRD="";
        $scope.parties.PRSC="";
        $scope.parties.Valid ="";
      }
    });

  }
  function validVotes() {
    return $scope.parties.PLD + $scope.parties.PRD + $scope.parties.PRSC == $scope.parties.Valid
  }
  $scope.cleanForm = function () {
    $scope.parties = {}
  }

}).controller('menuCtrl',function ($scope) {

}).controller ('concurrentesCtrl',function ($scope, $ionicPopup) {


  $scope.parties = {

  };
  $scope.enviar = function () {
    console.log($scope.parties.Municipio)
    var message = "Votatens enviados exitosamente...!!";
    var title = 'Enviado <i class="icon ion-android-checkbox-outline"></i>'
    var valid = true;
    if($scope.parties.Municipio <=0) {
      message = "Codigo de municipio incorrecto";
      title = 'Error <i style="color:#ff3b30" class="icon ion-close-circled"></i>';
      valid = false;
    }else if ($scope.parties.Municipio <=0){
      message = "Colegio incorrecto";
      title = 'Error <i style="color:#ff3b30" class="icon ion-close-circled"></i>'
    }else if ($scope.parties.Municipio <=0){
      message = "Codigo Verificador incorrecto.";
      title = 'Error <i style="color:#ff3b30" class="icon ion-close-circled"></i>';
      valid = false;
    }else if  (!validVotes()) {
      message = "Debe enviar por lo menos 1 votante";
      title = 'Error <i style="color:#ff3b30" class="icon ion-close-circled"></i>';
      valid = false;
    }
    console.log(validVotes());
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message
    });

    alertPopup.then(function(res) {
      if ( valid ) {
        console.log('Se ha enviado el .');
        $scope.parties.PLD="";
        $scope.parties.PRD="";
        $scope.parties.PRSC="";
        $scope.parties.Valid ="";
      }
    });

  }
  function validVotes() {

    return $scope.parties.PLD > 0 || $scope.parties.PRD > 0 ||$scope.parties.PRSC > 0
  }
  $scope.cleanForm = function () {
    $scope.parties = {}
  }

});
